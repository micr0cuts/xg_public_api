import unittest
from unittest.mock import MagicMock
from unittest.mock import patch
import time
import os

from xg_query_api.token import Token, AuthError

class TestToken(unittest.TestCase):

    def setUp(self):
        self.malformed_token = {"malformed": "token"}
        self.expired_refresh_token = {
            "refreshTokenTime": 1, 
            "accessTokenTime": time.time() + 1000
        }
        self.expired_access_token = {
            "accessTokenTime": 1, 
            "refreshTokenTime": time.time() + 1000
        }
        return

    def test_throws_error_when_username_isnt_set(self):
        mock_get_token = MagicMock(
                return_value="")

        token = Token()
        token.email = None
        token.pwd = "Something"
        token._get_token = mock_get_token

        with self.assertRaises(AuthError):
            token.get_token()
        return

    def test_throws_error_when_passowrd_isnt_set(self):
        mock_get_token = MagicMock(
                return_value="")

        token = Token()
        token.email = "Something"
        token.pwd = None
        token._get_token = mock_get_token

        with self.assertRaises(AuthError):
            token.get_token()
        return

    @patch.dict(os.environ, {"XG_EMAIL": "Test"})
    def test_uses_passed_values_for_user_over_env(self):
        token = Token("Real Value", "FakePwd")
        self.assertTrue(token.email == "Real Value")
        return

    def test_defaults_to_api_xg_football_without_xg_location_env(self):
        token = Token()
        self.assertTrue(token.base == "https://api.xg.football")
        return

    ##Token exists but is either expired or malformed

    def test_will_refetch_on_expired_refresh_token(self):

        mock_read_token = MagicMock(
                return_value=self.expired_refresh_token)
        mock_token_is_malformed = MagicMock(
                return_value=False)
        mock_fetch_new_token_and_write = MagicMock(
                return_value=None)

        token = Token()
        token.token_exists = True
        token.email=True
        token.pwd=True

        token._read_token = mock_read_token
        token._token_is_malformed = mock_token_is_malformed
        token._fetch_new_token_and_write = mock_fetch_new_token_and_write

        token.get_token()

        mock_fetch_new_token_and_write.assert_called_once()
        return


    def test_will_refetch_on_expired_access_token(self):

        mock_read_token = MagicMock(
                return_value=self.expired_access_token)
        mock_token_is_malformed = MagicMock(
                return_value=False)
        mock_fetch_new_token_and_write = MagicMock(
                return_value=None)

        token = Token()
        token.token_exists = True
        token.email=True
        token.pwd=True

        token._read_token = mock_read_token
        token._token_is_malformed = mock_token_is_malformed
        token._fetch_new_token_and_write = mock_fetch_new_token_and_write

        res = token.get_token()

        mock_fetch_new_token_and_write.assert_called_once()

    def test_removes_and_fetches_malformed_token(self):

        mock_read_token = MagicMock(
                return_value=self.malformed_token)

        mock_remove_token = MagicMock(
                return_value=None)
        mock_fetch_token = MagicMock(
                return_value={"token": "token"})
        mock_write_token = MagicMock(
                return_value=None)

        token = Token()
        token.token_exists=True
        token.email=True
        token.pwd=True

        token._read_token = mock_read_token
        token._remove_token = mock_remove_token
        token._fetch_token = mock_fetch_token
        token._write_token = mock_write_token

        token.get_token()

        mock_remove_token.assert_called_once()
        mock_fetch_token.assert_called_once()
        mock_write_token.assert_called_once()
        self.assertTrue(mock_read_token.call_count == 2)
        return

    ##Token doesn't exist

    def test_token_will_create_when_not_found(self):
        mock_fetch_new_token_and_write = MagicMock(
                return_value=None)
        mock_read_token = MagicMock(
                return_value=None)

        token = Token()
        token.token_exists = False
        token.email=True
        token.pwd=True

        token._fetch_new_token_and_write = mock_fetch_new_token_and_write
        token._read_token = mock_read_token

        res = token.get_token()

        mock_fetch_new_token_and_write.assert_called_once()
        return

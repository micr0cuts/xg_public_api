import os
import json
import time
import requests

class AuthError(Exception):

    def __init__(self, missing):
        self.missing = missing

    def __str__(self):
        return "Missing " + self.missing


class Token:

    location = os.getcwd() + '/token/token.json' if os.environ.get("XG_TOKEN_LOCATION") is None else os.environ.get("XG_TOKEN_LOCATION")

    def __init__(self, email=None, pwd=None):
        self.base = 'https://api.xg.football' if os.environ.get("XG_LOCATION") is None else os.environ.get("XG_LOCATION")
        self.token_exists = os.path.isfile(Token.location)
        self.email = email if email else os.environ.get("XG_EMAIL")
        self.pwd = pwd if pwd else os.environ.get("XG_PWD")
        self.accessExpires = time.time() + 3500
        self.refreshExpires = time.time() + (86400 * 6)
        return

    def _fetch_token(self, existing_token = None):
        
        url = self.base + "/user/login"
        payload = {
            'email': self.email, 
            'password': self.pwd
        }

        r = requests.post(
            url,
            verify = False,
            json = payload
        )
        res = r.json()
        res['accessTokenTime'] = self.accessExpires
        # if token is passed, then we are just refreshing
        res['refreshTokenTime'] = existing_token['refreshTokenTime'] if existing_token else self.refreshExpires
        return res

    def _read_token(self):
        #Shouldn't be called if file doesn't exist
        with open(Token.location, 'r') as f:
            return json.load(f)

    def _remove_token(self):
        os.remove(Token.location)

    def _write_token(self, token):
        with open(Token.location, 'w') as f:
            f.write(json.dumps(token))

    def _fetch_new_token_and_write(self, existing_token = None):
        token = self._fetch_token(existing_token)
        self._write_token(token)
        return

    def _token_is_malformed(self, token):
        return 'message' in token \
            or 'refreshTokenTime' not in token \
            or 'accessTokenTime' not in token

    def _refresh_token_is_expired(self, token):
        return time.time() > token['refreshTokenTime']

    def _access_token_is_expired(self, token):
        return time.time() > token['accessTokenTime']

    def get_token(self):

        if not self.email:
            raise AuthError("email")

        if not self.pwd:
            raise AuthError("pwd")

        if self.token_exists:
            token = self._read_token()

            if self._token_is_malformed(token):
                self._remove_token()
                self._fetch_new_token_and_write()
            else:
                ##We are assuming it isn't possible to fetch
                ##an out-of-date token
                if self._refresh_token_is_expired(token):
                    self._fetch_new_token_and_write()
                elif self._access_token_is_expired(token):
                    self._fetch_new_token_and_write(token)

        else:
            self._fetch_new_token_and_write()

        return self._read_token()


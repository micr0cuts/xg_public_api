import json
import time

from xg_query_api.xg import API

class Getter:
    
    def fetch(self, url):
        time.sleep(1)

        if self.email and self.pwd:
            return API(self.email, self.pwd).auth_request(url)
        else:
            return API().request(url)

    def __init__(self, email, pwd):
        self.email = email
        self.pwd = pwd
        return
